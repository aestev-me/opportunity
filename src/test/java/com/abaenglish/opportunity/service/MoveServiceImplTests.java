package com.abaenglish.opportunity.service;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.abaenglish.opportunity.domain.Planet;
import com.abaenglish.opportunity.domain.Position;
import com.abaenglish.opportunity.service.impl.MoveServiceImpl;

@RunWith(SpringRunner.class)
public class MoveServiceImplTests {
	
	@Autowired
	MoveServiceImpl moveServiceImpl;
	
	@TestConfiguration
    static class MoveServiceImplTestContextConfiguration {
  
        @Bean
        public MoveServiceImpl moveService() {
            return new MoveServiceImpl();
        }
    }

	@Test
	public void move() throws Exception{
		Position position = new Position(1, 2, 'N');
		
		Position newPos = moveServiceImpl.move(position);
		Assert.assertNotNull(newPos);
		
		position.setY(1);
		Assert.assertTrue(position.equals(newPos));
		
		position = new Position(1, 2, 'E');
		newPos = moveServiceImpl.move(position);
		position.setX(2);
		Assert.assertTrue(position.equals(newPos));
		
		position = new Position(1, 2, 'S');
		newPos = moveServiceImpl.move(position);
		position.setY(3);
		Assert.assertTrue(position.equals(newPos));
		
		position = new Position(1, 2, 'W');
		newPos = moveServiceImpl.move(position);
		position.setX(0);
		Assert.assertTrue(position.equals(newPos));
	}
	
	@Test(expected=NullPointerException.class)
	public void movePositionNull() throws Exception{
		moveServiceImpl.move(null);
		fail("should've thrown an NullPointerException");
	}
	
	@Test(expected=Exception.class)
	public void movePositionEmpty() throws Exception{
		Position position = new Position();
		try {
			moveServiceImpl.move(position);
		} catch (Exception e) {
			Assert.assertEquals("The direction must be 'N', 'E', 'S' or 'W'", e.getMessage());
			throw e;
		}
		fail("should've thrown an Exception");
	}
	
	@Test(expected=Exception.class)
	public void moveDirectionEmpty() throws Exception{
		Position position = new Position();
		position.setX(1);
		position.setY(1);
		try {
			moveServiceImpl.move(position);
		} catch (Exception e) {
			Assert.assertEquals("The direction must be 'N', 'E', 'S' or 'W'", e.getMessage());
			throw e;
		}
		fail("The direction must be 'N', 'E', 'S' or 'W'");
	}
	
	@Test
	public void canMoveTrue(){
		Position position = new Position(1, 2, 'N');
		Planet planet = new Planet(5, 5);
		Assert.assertTrue(moveServiceImpl.canMove(position, planet));
	}
	
	@Test
	public void canMoveFalse(){
		Position position = new Position(5, 5, 'N');
		Planet planet = new Planet(1, 2);
		Assert.assertFalse(moveServiceImpl.canMove(position, planet));
	}
	
	@Test(expected=NullPointerException.class)
	public void canMovePositionNull(){
		Planet planet = new Planet(1, 2);
		moveServiceImpl.canMove(null, planet);
		fail("should've thrown an NullPointerException");
	}
	
	@Test(expected=NullPointerException.class)
	public void canMovePlanetNull(){
		Position position = new Position(1, 2, 'N');
		moveServiceImpl.canMove(position, null);
		fail("should've thrown an NullPointerException");
	}
	
	@Test(expected=NullPointerException.class)
	public void canMoveParametersNull(){
		moveServiceImpl.canMove(null, null);
		fail("should've thrown an NullPointerException");
	}
}
