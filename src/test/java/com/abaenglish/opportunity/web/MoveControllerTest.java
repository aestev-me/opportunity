package com.abaenglish.opportunity.web;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.abaenglish.opportunity.domain.MoveRequest;
import com.abaenglish.opportunity.domain.Planet;
import com.abaenglish.opportunity.domain.Position;
import com.abaenglish.opportunity.service.MoveService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(MoveController.class)
public class MoveControllerTest {
	@Autowired
    private MockMvc mockMvc;
	@Autowired 
	private ObjectMapper mapper;
	
	@MockBean
	MoveService moveService;
	
	@Test
	public void executeMove() throws Exception {
		Position position = new Position(1, 2, 'N');
		Planet planet = new Planet(5,5);
		Mockito.when(moveService.canMove(ArgumentMatchers.any(Position.class), ArgumentMatchers.any(Planet.class))).thenReturn(true);
		Mockito.when(moveService.move(ArgumentMatchers.any(Position.class))).thenReturn(new Position(1, 1, 'N'));
		String json = mapper.writeValueAsString(new MoveRequest(position, planet));
		this.mockMvc.perform(post("/move")
			       .contentType(MediaType.APPLICATION_JSON)
			       .content(json)
			       .accept(MediaType.APPLICATION_JSON))
			       .andExpect(status().isOk())
			       .andExpect(MockMvcResultMatchers.jsonPath("$.position").isNotEmpty());
	}
	
	@Test(expected=NullPointerException.class)
	public void executeMoveRequestNull() throws Exception {
		String json = null;
		this.mockMvc.perform(post("/move")
			       .contentType(MediaType.APPLICATION_JSON)
			       .content(json)
			       .accept(MediaType.APPLICATION_JSON))
			       .andExpect(status().isOk());
		fail("should've thrown an NullPointerException");
	}
	
	@Test
	public void executeMoveRequestEmpty() throws Exception {
		String json = mapper.writeValueAsString(new MoveRequest());
		Mockito.when(moveService.canMove(ArgumentMatchers.any(Position.class), ArgumentMatchers.any(Planet.class))).thenReturn(true);
		Mockito.when(moveService.move(ArgumentMatchers.any(Position.class))).thenReturn(new Position());
		this.mockMvc.perform(post("/move")
			       .contentType(MediaType.APPLICATION_JSON)
			       .content(json)
			       .accept(MediaType.APPLICATION_JSON))
			       .andExpect(status().isOk())
			       .andExpect(MockMvcResultMatchers.jsonPath("$.error").value("The data received is not correct, re-analyze territory(Planet) and position"));
	}

}