package com.abaenglish.opportunity.domain;

public class TransmitReponse {
	private String data;

	public TransmitReponse(String data) {
		super();
		this.data = data;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
}
