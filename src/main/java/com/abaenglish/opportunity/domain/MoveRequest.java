package com.abaenglish.opportunity.domain;

public class MoveRequest {
	private Position position;
	private Planet planet;

	public MoveRequest() {
		super();
	}

	public MoveRequest(Position position, Planet planet) {
		super();
		this.position = position;
		this.planet = planet;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	
}
