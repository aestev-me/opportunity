package com.abaenglish.opportunity.domain;

public class MoveResponse {
	private Position position;
	private String error;
	public MoveResponse() {
		super();
	}
	public MoveResponse(Position position, String error) {
		super();
		this.position = position;
		this.error = error;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

}
