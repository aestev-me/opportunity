package com.abaenglish.opportunity.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.abaenglish.opportunity.domain.Planet;
import com.abaenglish.opportunity.domain.Position;
import com.abaenglish.opportunity.service.MoveService;

@Service
public class MoveServiceImpl implements MoveService {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MoveServiceImpl.class);

	@Override
	public Position move(Position position) throws Exception {
		switch (position.getDirection()) {
	        case 'N':
	        	position.setY(position.getY() - 1);
	            break;
	        case 'E':
	        	position.setX(position.getX() + 1);
	            break;
	        case 'S':
	        	position.setY(position.getY() + 1);
	            break;
	        case 'W':
	        	position.setX(position.getX() - 1);
	            break;
	        default:
	        	throw new Exception("The direction must be 'N', 'E', 'S' or 'W'");
		}
		return position;
	}

	@Override
	public boolean canMove(Position position, Planet planet) {
		boolean result = true;
		if(position.getX() < 0 || position.getY() < 0) {
			result = false;
			LOGGER.debug("The current coordinates can not be negative.");
		}else if(planet.getX() < 0 || planet.getY() < 0) {
			result = false;
			LOGGER.debug("The coordinates of the scanned territory can not be negative.");
		}else if(position.getX() > planet.getX() || position.getY() > planet.getY()){
			result = false;
			LOGGER.debug("The coordinates are outside the scanned territory can not be negative.");
		}
		return result;
	}

}
