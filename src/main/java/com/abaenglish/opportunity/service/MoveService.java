package com.abaenglish.opportunity.service;

import com.abaenglish.opportunity.domain.Planet;
import com.abaenglish.opportunity.domain.Position;


public interface MoveService {
	Position move(Position position) throws Exception;
	boolean canMove(Position position, Planet planet);
}
