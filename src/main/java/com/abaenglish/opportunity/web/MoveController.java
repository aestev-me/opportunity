package com.abaenglish.opportunity.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abaenglish.opportunity.domain.MoveRequest;
import com.abaenglish.opportunity.domain.MoveResponse;
import com.abaenglish.opportunity.domain.Planet;
import com.abaenglish.opportunity.domain.Position;
import com.abaenglish.opportunity.service.MoveService;

@RestController
public class MoveController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MoveController.class);
	
	@Autowired
	MoveService moveService;
	
	
	@RequestMapping(path = "/move", method= RequestMethod.POST)
	public MoveResponse executeMove(@RequestBody MoveRequest moveRequest) {
		Position currentPos = moveRequest.getPosition();
		Planet planet = moveRequest.getPlanet();
		Position newPos;
		MoveResponse moveResponse = null;
		if(currentPos != null && planet != null && moveService.canMove(currentPos, planet)){
			try {
				newPos = moveService.move(currentPos);
				moveResponse = new MoveResponse(newPos, null);
			} catch (Exception e) {
				LOGGER.error("Error when executing the movement: ", e);
				moveResponse = new MoveResponse(null, e.getMessage());
			}
		} else {
			LOGGER.debug("The data received is not correct, re-analyze territory(Planet) and position");
			moveResponse = new MoveResponse(null, "The data received is not correct, re-analyze territory(Planet) and position");
		}
        return moveResponse;
	}
}
