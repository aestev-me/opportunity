package com.abaenglish.opportunity.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abaenglish.opportunity.domain.TransmitReponse;

@RestController
public class TransmitController {
	@RequestMapping(path = "/texts", method= RequestMethod.GET)
	public TransmitReponse getTexts() {
        return new TransmitReponse("Example");
	}

}
